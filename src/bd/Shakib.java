package bd;
import robocode.*;
import java.awt.*;
import static robocode.util.Utils.normalRelativeAngleDegrees;

/**
 *
 */
public class Shakib extends AdvancedRobot {
    int direction=1;//which way to move
    int movement=1;
    boolean peek; // Don't turn if there's a robot there
    double moveAmount; // How much to move
    int i=0;
    boolean oneToOne;


    /**
     * run:
     */
    public void run() {
        setBodyColor(Color.green);
        setGunColor(Color.red);
        setRadarColor(Color.black);
        setBulletColor(Color.green);
        setScanColor(Color.black);

        if(getOthers()>3){
            System.out.println("More than 1 enemy");
            oneToOne = false;
            // Initialize moveAmount to the maximum possible for this battlefield.
            moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
            System.out.println("Move Amount: " + moveAmount);
            // Initialize peek to false
            peek = false;
            turnLeft(getHeading() % 90);
            ahead(moveAmount);
            // Turn the gun to turn right 90 degrees.
            peek = true;
            turnGunRight(90);
            turnRight(90);

            while (true) {
                // Look before we turn when ahead() completes.
                if (getOthers() < 4) {
                    oneToOne=true;
                    break;
                }
              /*  if (getX() == 18.0 || getY() == 18.0 || getX() == 782.0 || getY() == 582.0) {
                    System.out.println("Over the Wall");
                    peek = true;
                    // check surrounding
                    turnGunRight(90);
                    turnGunLeft(180);
                    turnGunRight(90);
                    // Move up the wall
                    ahead(moveAmount / 2);
                    // Don't look now
                    peek = false;
                    i++;
                    if (i == 2) {
                        turnRight(90);
                        i = 0;
                    }
                } else {*/
                    peek = true;
                    // Move up the wall
                    ahead(moveAmount);
                    // Don't look now
                    peek = false;
                    // Turn to the next wall
                    turnRight(90);
                //}
            }
        }

            oneToOne = true;
            setAdjustRadarForRobotTurn(true);//keep the radar still while we turn
            setAdjustGunForRobotTurn(true); // Keep the gun still when we turn
            turnRadarRightRadians(Double.POSITIVE_INFINITY);//keep turning radar right

    }

    /**
     * Scan other robot
     */
    public void onScannedRobot(ScannedRobotEvent e) {
        if(oneToOne==false) {
            fire(2);
            // Note that scan is called automatically when the robot is moving.
            // By calling it manually here, we make sure we generate another scan event if there's a robot on the next
            // wall, so that we do not start moving up it until it's gone.
            if (peek) {
                scan();
            }
        }
        else if(oneToOne==true) {
            System.out.println("second line");
            double turnGun;//amount to turn our gun
            double absoluteBearing = e.getBearingRadians() + getHeadingRadians();//enemies absolute bearing
            double expectedVelocity = e.getVelocity() * Math.sin(e.getHeadingRadians() - absoluteBearing);//enemies later velocity
            //double gunTurnAmt;//amount to turn our gun
            setTurnRadarLeftRadians(getRadarTurnRemainingRadians());//lock on the radar
            if (Math.random() > .9) {
                setMaxVelocity((12 * Math.random()) + 12);//randomly change speed
            }
            if (e.getDistance() > 150) {
                turnGun = robocode.util.Utils.normalRelativeAngle(absoluteBearing - getGunHeadingRadians() + expectedVelocity / 22);//amount to turn our gun, lead just a little bit
                setTurnGunRightRadians(turnGun); //turn our gun
                setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absoluteBearing - getHeadingRadians() + expectedVelocity / getVelocity()));//drive towards the enemies predicted future location
                setAhead((e.getDistance() - 140) * direction);//move forward
                setFire(3);//fire
            } else {
                turnGun= robocode.util.Utils.normalRelativeAngle(absoluteBearing - getGunHeadingRadians() + expectedVelocity / 15);//amount to turn our gun, lead just a little bit
                setTurnGunRightRadians(turnGun);//turn our gun
                setTurnLeft(-90 - e.getBearing()); //turn perpendicular to the enemy
                setAhead((e.getDistance() - 140) * direction);//move forward
                setFire(3);//fire
            }
        }
    }

    public void onHitRobot(HitRobotEvent e) {

        if (e.getBearing() > -90 && e.getBearing() < 90) {
            back(150);
           //movement*=(-movement);

        } // else he's in back of us, so set ahead a bit.
        else {
            ahead(150);
           // movement*=(-movement);

        }

    }
   /* public void onHitByBullet(HitByBulletEvent e) {


        ahead(100);


    }*/


    public void onHitWall(HitWallEvent e){
        if(oneToOne == true)
        direction=-direction;//reverse direction upon hitting a wall
    }
    /**
     * onWin:  Do a victory dance
     */
    public void onWin(WinEvent e) {
        for (int i = 0; i < 50; i++) {
            turnRight(30);
            turnLeft(30);
        }
    }}

